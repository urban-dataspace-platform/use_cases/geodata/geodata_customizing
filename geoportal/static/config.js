const Config = {
    alerting: {},
    wfsImgPath: "./resources/img/",
    cswId: "004",
    namedProjections: [
        ["EPSG:25833", "+title=ETRS89/UTM 33N +proj=utm +zone=33 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"],
        ["EPSG:4326", "+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"],
        ["EPSG:25832", "+title=ETRS89/UTM 32N +proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"]
    ],
    footer: {
        urls: [
        {
            "url": "https://www.hypertegrity.de/impressum/",
            "alias": "Bereitgestellt von Hypertegrity AG",
            "alias_mobil": "HTAG"
        },
        {
            "url": "mailto:info@hypertegrity.de",
            "alias": "Fehler melden"
        }

    ],
        showVersion: true
    },
    ignoredKeys: ["BOUNDEDBY", "SHAPE", "SHAPE_LENGTH", "SHAPE_AREA", "OBJECTID", "GLOBALID", "GEOMETRY", "SHP", "SHP_AREA", "SHP_LENGTH","GEOM", "Fid", "Id", "Id_0", "X", "Y", "Extent"],
    quickHelp: {
        imgPath: "./resources/img/"
    },
    layerConf: "./resources/services-internet.json",
    restConf: "./resources/rest-services-internet.json",
    styleConf: "./resources/style_v3.json",
    scaleLine: true,
    mouseHover: {
        numFeaturesToShow: 2,
        infoText: "(weitere Objekte. Bitte zoomen.)"
    },
    useVectorStyleBeta: true, 
    portalLanguage: {
        enabled: true,
        debug: false,
        languages: {
            de: "Deutsch",
            en: "English",
            es: "Español",
            it: "Italiano",
            pt: "Português"
        },
        fallbackLanguage: "de",
        changeLanguageOnStartWhen: ["querystring", "localStorage", "htmlTag"],
        loadPath: "mastercode/3_7_0/locales/{{lng}}/{{ns}}.json"
    },
    login: {
        oidcAuthorizationEndpoint: "https://idm.{{ DOMAIN }}/auth/realms/{{ REALM }}/protocol/openid-connect/auth",
        oidcTokenEndpoint: "https://idm.{{ DOMAIN }}/auth/realms/{{ REALM }}/protocol/openid-connect/token",
        oidcClientId: "public_masterportal",
        oidcScope: "profile email openid",
        oidcRedirectUri: "https://geoportal.{{ DOMAIN }}/portal",
        interceptorUrlRegex: "https?://geoportal.{{ DOMAIN }}/*" // add authorization to all URLs that match the given regex
    },
};

// conditional export to make config readable by e2e tests
if (typeof module !== "undefined") {
    module.exports = Config;
}
